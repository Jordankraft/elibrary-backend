-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : jeu. 23 juin 2022 à 10:08
-- Version du serveur : 10.4.19-MariaDB
-- Version de PHP : 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `eLibrary`
--

-- --------------------------------------------------------

--
-- Structure de la table `Corrections`
--

CREATE TABLE `Corrections` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `contenu` varchar(255) NOT NULL,
  `objectif` text DEFAULT NULL,
  `etat` varchar(255) DEFAULT NULL,
  `nbTelechargement` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `idEnseignantPublieur` int(11) DEFAULT NULL,
  `EnseignantId` int(11) DEFAULT NULL,
  `idEnseignantSupprimeur` int(11) DEFAULT NULL,
  `TdId` int(11) DEFAULT NULL,
  `EpreuveId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `DocumentDomaine`
--

CREATE TABLE `DocumentDomaine` (
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `DomaineId` int(11) NOT NULL,
  `DocumentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `Documents`
--

CREATE TABLE `Documents` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `contenu` varchar(255) NOT NULL,
  `resume` text DEFAULT NULL,
  `etat` varchar(255) NOT NULL DEFAULT 'actif',
  `nbTelechargement` int(11) NOT NULL DEFAULT 0,
  `auteur` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `TypeId` int(11) DEFAULT NULL,
  `FaculteId` int(11) DEFAULT NULL,
  `FiliereId` int(11) DEFAULT NULL,
  `NiveauId` int(11) DEFAULT NULL,
  `SpecialiteId` int(11) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  `UeId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Documents`
--

INSERT INTO `Documents` (`id`, `titre`, `contenu`, `resume`, `etat`, `nbTelechargement`, `auteur`, `createdAt`, `updatedAt`, `TypeId`, `FaculteId`, `FiliereId`, `NiveauId`, `SpecialiteId`, `UserId`, `UeId`) VALUES
(2, 'introduction a l\'algorithmiue', 'file.pdf', 'cours sur lalgoritmque', 'actif', 1, 'Dr. Kimbi', '2022-05-31 10:05:21', '2022-06-22 15:40:17', 3, 1, 1, 2, NULL, NULL, NULL),
(3, 'introduction a l\'algorithmiue', 'file.pdf', 'cours sur lalgoritmque', 'bloqué', 4, 'Dr. Kimbi', '2022-05-31 10:07:12', '2022-06-10 13:14:20', 3, 1, 1, 3, 1, NULL, NULL),
(4, 'introduction a l\'algorithmiue', 'file.pdf', 'cours sur lalgoritmque', 'bloqué', 9, 'Dr. Kimbi', '2022-05-31 10:08:01', '2022-06-10 18:07:49', 3, 1, 1, 3, 1, 26, NULL),
(5, 'software testing', 'document.pdf', 'software testing', 'supprimé', 0, 'Dr Kimbi', '2022-06-06 11:11:04', '2022-06-06 11:11:04', 3, 1, 1, 4, 2, 26, NULL),
(6, 'assurance quality', 'INF3075 PPT CFG Version.pdf', 'software testing and assurance quality. Make sure that the system work well according to his specifications', 'actif', 8, NULL, '2022-06-06 12:45:28', '2022-06-18 22:36:31', 1, 1, 2, 5, 4, 26, NULL),
(7, 'software testing', 'document.pdf', 'software testing', 'actif', 1, NULL, '2022-06-06 13:46:17', '2022-06-06 16:50:31', 2, 1, 1, 3, 1, 57, NULL),
(8, 'software testing', 'INF3075 PPT CFG Version.pdf', 'software testing', 'actif', 0, 'Dr Kimbi', '2022-06-06 13:47:39', '2022-06-06 13:47:39', 2, 1, 1, 3, 1, 57, NULL),
(9, 'software testing', 'INF3075 PPT CFG Version.pdf', 'software testing', 'actif', 0, 'Dr Kimbi', '2022-06-06 13:48:15', '2022-06-06 13:48:15', 2, 1, 1, 3, 1, 57, NULL),
(10, 'software testing', 'INF3075 PPT CFG Version.pdf', 'software testing', 'actif', 2, 'Dr Zefack', '2022-06-06 13:48:47', '2022-06-06 16:51:43', 2, 1, 3, 2, 6, 57, NULL),
(12, 'software testing and quality assurance', 'INF3075 PPT New Version.pdf', 'introduction of software testing', 'actif', 4, 'Kimbi Xaveria', '2022-06-13 15:47:18', '2022-06-15 11:19:32', 2, 2, 1, 3, 1, 67, 3),
(13, 'Software testing TD', 'Software testing and quality assurance TD.pdf', 'first TD', 'actif', 0, 'Kimbi Xaveria', '2022-06-13 15:47:18', '2022-06-13 15:47:18', 4, 2, 1, 3, 1, 67, 3),
(14, 'software testing and quality assurance', 'INF3075 PPT New Version.pdf', 'introduction of software testing', 'actif', 0, 'Kimbi Xaveria', '2022-06-13 15:47:18', '2022-06-13 15:47:18', 2, 2, 1, 3, 1, 67, 3),
(15, 'Software testing TD', 'Software testing and quality assurance TD.pdf', 'first TD', 'actif', 0, 'Kimbi Xaveria', '2022-06-13 15:47:18', '2022-06-13 15:47:18', 4, 2, 1, 3, 1, 67, 3),
(16, 'software testing and quality assurance', 'INF3075 PPT New Version.pdf', 'introduction of software testing', 'actif', 0, 'Kimbi Xaveria', '2022-06-13 15:47:18', '2022-06-13 15:47:18', 2, 2, 1, 3, 1, 67, 3),
(17, 'Software testing TD', 'Software testing and quality assurance TD.pdf', 'first TD', 'actif', 0, 'Kimbi Xaveria', '2022-06-13 15:47:18', '2022-06-13 15:47:18', 4, 2, 1, 3, 1, 67, 3),
(18, 'Pratique des tests logiciel', 'Pratique des tests logiciels - - Jean-Francois Pradat-Peyre.pdf', 'introduction et pratiques des tests logiciel', 'actif', 1, 'Kimbi Xaveria', '2022-06-13 15:55:15', '2022-06-22 18:38:37', 3, 1, 1, 3, 1, 26, NULL),
(20, 'Black box Testing', 'Black box Testing.pdf', 'introduction of black box testing', 'actif', 0, 'Kimbi Xaveria', '2022-06-13 16:05:21', '2022-06-13 16:05:21', 2, 2, 1, 3, 1, 67, 3),
(21, 'introduction a la COO', 'INF3055_Support_1_Intro-Gene_21-22-1.pdf', 'introduction générale à la conception orienté objet ', 'actif', 0, 'Monthé Valéry', '2022-06-13 17:31:14', '2022-06-13 17:31:14', 2, 2, 1, 3, 1, 62, 4),
(22, 'Principes SOLID', 'INF3055_Support_2_Principes_SOLID_21-22.pdf', 'les principes SOLID', 'actif', 0, NULL, '2022-06-13 17:36:57', '2022-06-13 17:36:57', 2, 2, 1, 3, 1, 62, 4),
(23, 'Langage UML', 'INF3055_Support_3_Langage_UML_21-22-2.pdf', NULL, 'actif', 0, 'Monthé Valéry', '2022-06-13 17:38:45', '2022-06-13 17:38:45', 2, 2, 1, 3, 1, 62, 4),
(24, 'TD1 Principes de base POO', 'TD_1_Principes-de-base-POO_INF3055_21-22.pdf', 'td sur les principes de base de la POO', 'actif', 0, 'Monthé Valéry', '2022-06-13 17:40:35', '2022-06-13 17:40:35', 4, 2, 1, 3, 1, 62, 4),
(25, 'TD2 principes SOLID', 'TD_2_Principes-SOLID_INF3055_21-22.pdf', NULL, 'actif', 0, NULL, '2022-06-13 17:45:38', '2022-06-13 17:45:38', 4, 2, 1, 3, 1, 62, 4),
(26, 'TD3 Modelisation UML', 'TD_3_Modélisation-UML_INF3055_21-22.pdf', NULL, 'actif', 0, 'Monthé Valéry', '2022-06-13 17:49:30', '2022-06-13 17:49:30', 4, 2, 1, 3, 1, 62, 4),
(27, 'Introduction au génie logiciel ', 'Chapitre 1 et 2 GL.pdf', NULL, 'actif', 0, 'Abessolo Ghislain', '2022-06-13 17:58:14', '2022-06-13 17:58:14', 2, 2, 1, 3, 1, 64, 5),
(28, 'Les Progiciels de gestion intégré', 'Cours 2 les PGI.pdf', NULL, 'actif', 0, 'Abessolo Ghislain', '2022-06-13 18:03:43', '2022-06-13 18:03:43', 2, 2, 1, 3, 1, 65, 5),
(29, 'Planification d\'un projet', 'GestionProjet Planification ABESSOLO.ppt.pdf', 'planification et gestion des risques pour un projet', 'actif', 1, 'Abessolo Ghislain', '2022-06-13 18:13:53', '2022-06-20 10:30:25', 2, 1, 1, 5, 1, 62, 5),
(30, 'mon doc', 'Presentation_-_TP_INF3196.pdf', 'pour le teste', 'actif', 4, 'Adams', '2022-06-14 17:36:35', '2022-06-22 10:33:33', 3, 1, 1, 3, 1, 69, NULL),
(31, 'doc2', 'document.pdf', '2e teste', 'actif', 0, 'Adams', '2022-06-14 17:42:45', '2022-06-14 17:42:46', 1, 1, 1, 2, 1, 69, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Domaines`
--

CREATE TABLE `Domaines` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `UserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Domaines`
--

INSERT INTO `Domaines` (`id`, `nom`, `createdAt`, `updatedAt`, `UserId`) VALUES
(1, 'science', '2022-05-31 11:29:57', '2022-05-31 11:29:57', 26);

-- --------------------------------------------------------

--
-- Structure de la table `Epreuves`
--

CREATE TABLE `Epreuves` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `contenu` varchar(255) NOT NULL,
  `objectif` text DEFAULT NULL,
  `etat` varchar(255) DEFAULT NULL,
  `nbTelechargement` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `idEnseignantPublieur` int(11) DEFAULT NULL,
  `EnseignantId` int(11) DEFAULT NULL,
  `idEnseignantSupprimeur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `Facultes`
--

CREATE TABLE `Facultes` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Facultes`
--

INSERT INTO `Facultes` (`id`, `nom`, `createdAt`, `updatedAt`) VALUES
(1, 'facsciences', '2022-05-28 15:28:41', '2022-05-28 15:28:41'),
(2, 'faclettres', '2022-05-28 15:28:41', '2022-05-28 15:28:41');

-- --------------------------------------------------------

--
-- Structure de la table `filiereNiveau`
--

CREATE TABLE `filiereNiveau` (
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `NiveauId` int(11) NOT NULL,
  `SpecialiteId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `Filieres`
--

CREATE TABLE `Filieres` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `FaculteId` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Filieres`
--

INSERT INTO `Filieres` (`id`, `nom`, `createdAt`, `updatedAt`, `FaculteId`, `description`) VALUES
(1, 'Informatique', '2022-05-28 15:29:44', '2022-05-28 15:29:44', 1, 'En fonction de votre spécialité souhaitée (Génie logiciel, sciences de données, sécurité, réseaux, etc...) Vous serez capable de concevoir et construire des systèmes complexes pour le traitement de l\'information'),
(2, 'Mathematiques', '2022-05-28 15:29:44', '2022-05-28 15:29:44', 1, 'Cette filière vous permet d\'acquérir des connaissances approfondies en mathématique (algèbre, géométrie, analyse, calcul différentiel et intégral, probabilités et statistiques, etc...) et aussi une formation scientifique pluridisciplinaire (Physique, chimie, informatique,...)'),
(3, 'bios', '2022-05-28 15:31:00', '2022-05-28 15:31:00', NULL, 'Dans ce domaine vous étudierez l\'ensemble des sciences de la vie, des biotes et du biome'),
(4, 'chimie', '2022-05-28 15:31:00', '2022-05-28 15:31:00', NULL, 'Avec cette filière omniprésente dans tous les stades de la vie vous étudierez la composition, les réactions et les propriétés de la matière'),
(5, 'physique', '2022-05-28 15:31:35', '2022-05-28 15:31:35', 1, 'Dans ce domaine vous étudierez la structure du monde matériel, de l\'infiniment petit (atome) à l\'infiniment grand (cosmos)'),
(6, 'LMF', '2022-05-28 15:31:35', '2022-05-28 15:31:35', 2, 'L\'objectif de cette filière est de vous offrir une formation globale dans les disciplines ayant trait à la création littéraire du moyen âge au XX1e siècle, à l\'histoire de la langue française, aux faits culturels, esthétiques et linguistiques'),
(7, 'geographie', '2022-05-28 15:32:02', '2022-05-28 15:32:02', 2, 'Ce domaine a pour objectif de vous amener à comprendre et expliquer la distribution spatiale des hommes et des sociétés actuelles, les processus de façonnement des territoires par les groupes sociaux ainsi que le fonctionnement des milieux dans lesquels ils vivent.'),
(8, 'sociologie', '2022-05-28 15:32:02', '2022-05-28 15:32:02', 2, 'Dans ce domaine vous étudierez les relations, actions et représentations sociales par lesquelles se constituent les sociétés');

-- --------------------------------------------------------

--
-- Structure de la table `Messages`
--

CREATE TABLE `Messages` (
  `id` int(11) NOT NULL,
  `contenu` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  `RequeteId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Messages`
--

INSERT INTO `Messages` (`id`, `contenu`, `createdAt`, `updatedAt`, `UserId`, `RequeteId`) VALUES
(1, 'Bonjour Dr.', '2022-06-21 04:33:50', '2022-06-21 04:33:50', 69, 1),
(2, 'J\'ai un problème sur l\'orthographe de mon prenom', '2022-06-21 04:33:51', '2022-06-21 04:33:51', 69, 1),
(3, 'Bonjour.', '2022-06-21 13:32:31', '2022-06-21 13:32:31', 26, 1),
(4, 'quel est votre problème?', '2022-06-21 13:33:34', '2022-06-21 13:33:34', 10, 2),
(5, 'bonjour Docteur. svp ', '2022-06-21 13:33:34', '2022-06-21 13:33:34', 69, 2),
(6, 'jai un probleme', '2022-06-21 15:33:33', '2022-06-21 15:33:33', 69, NULL),
(7, 'jai un probleme', '2022-06-21 15:36:22', '2022-06-21 15:36:22', 69, 2),
(8, 'il y\'a erreur sur mon nom', '2022-06-21 15:58:34', '2022-06-21 15:58:35', 69, 2),
(9, 'venez au bureau', '2022-06-21 17:06:07', '2022-06-21 17:06:07', 26, 1),
(10, 'd\'accord M.', '2022-06-21 17:13:57', '2022-06-21 17:13:57', 69, 1),
(11, 'quoi', '2022-06-21 17:19:01', '2022-06-21 17:19:01', 69, 1),
(12, 'rien', '2022-06-21 17:19:15', '2022-06-21 17:19:15', 26, 1),
(13, 'allo cava?', '2022-06-22 10:48:55', '2022-06-22 10:48:55', 69, 1),
(14, 'je vais bien et toi?', '2022-06-22 10:49:13', '2022-06-22 10:49:13', 26, 1),
(15, 'cc c\'est paulette', '2022-06-22 10:50:04', '2022-06-22 10:50:04', 26, 5),
(16, 'yo c\'est adams dans enseignant', '2022-06-22 10:50:50', '2022-06-22 10:50:50', 69, 2),
(17, 'adams à l\'enseignant', '2022-06-22 11:22:55', '2022-06-22 11:22:55', 69, 2),
(18, 'adams à lenseignant', '2022-06-22 11:25:52', '2022-06-22 11:25:53', 69, 2),
(19, 'adams à l\'enseignant mon doc', '2022-06-22 12:46:08', '2022-06-22 12:46:08', 69, 2),
(20, 'adams à paulette', '2022-06-22 12:47:09', '2022-06-22 12:47:09', 69, 1),
(21, 'paulette à adams', '2022-06-22 13:01:09', '2022-06-22 13:01:09', 26, 1),
(22, 'gghjkjhghjkjh', '2022-06-22 13:01:29', '2022-06-22 13:01:29', 69, 1),
(23, 'cvghjkjh', '2022-06-22 13:01:55', '2022-06-22 13:01:55', 69, 2),
(24, 'xdnjcjjs', '2022-06-22 13:04:53', '2022-06-22 13:04:54', 26, 1),
(25, 'xjsjsj', '2022-06-22 13:05:04', '2022-06-22 13:05:04', 26, 1),
(26, 'yeeess', '2022-06-22 13:17:58', '2022-06-22 13:17:58', 69, 1),
(27, 'yo', '2022-06-22 13:18:06', '2022-06-22 13:18:06', 26, 1),
(28, 'paulette à l\'enseignant', '2022-06-22 13:18:53', '2022-06-22 13:18:53', 26, 5),
(29, 'adams à l\'enseignant', '2022-06-22 13:19:06', '2022-06-22 13:19:06', 69, 2),
(30, 'jjsdhjdjd', '2022-06-22 13:20:16', '2022-06-22 13:20:16', 69, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Niveaus`
--

CREATE TABLE `Niveaus` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Niveaus`
--

INSERT INTO `Niveaus` (`id`, `nom`, `createdAt`, `updatedAt`, `description`) VALUES
(1, 'L1', '2022-05-28 15:37:13', '2022-05-28 15:37:13', 'Ici vous trouverez des documents d\'introduction dans votre domaine'),
(2, 'L2', '2022-05-28 15:37:13', '2022-05-28 15:37:13', 'Ce niveau correspond à des compétences moyennes et générales'),
(3, 'L3', '2022-05-28 15:37:46', '2022-05-28 15:37:46', 'En fonction de votre préférence vous trouverez des documents portant sur des domaines de spécialités'),
(4, 'M1', '2022-05-28 15:37:46', '2022-05-28 15:37:46', 'Faites des recherches pour préparer votre maîtrise'),
(5, 'M2', '2022-05-28 15:38:00', '2022-05-28 15:38:00', 'Retrouvez des anciennes thèses de Master pour vous inspirer'),
(6, 'D', '2022-05-28 15:38:00', '2022-05-28 15:38:00', 'Retrouvez des anciennes thèses de Doctorat pour vous inspirer');

-- --------------------------------------------------------

--
-- Structure de la table `Notifications`
--

CREATE TABLE `Notifications` (
  `id` int(11) NOT NULL,
  `contenu` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  `FiliereId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Notifications`
--

INSERT INTO `Notifications` (`id`, `contenu`, `createdAt`, `updatedAt`, `UserId`, `FiliereId`) VALUES
(1, 'premier notification', '2022-06-22 19:32:20', '2022-06-22 19:32:20', 69, 1),
(2, 'notifications pour toutes les filieres', '2022-06-22 19:40:52', '2022-06-22 19:40:52', 69, NULL),
(3, 'Notifications pour les bios', '2022-06-22 20:08:55', '2022-06-22 20:08:56', 69, 3);

-- --------------------------------------------------------

--
-- Structure de la table `PermissionRoles`
--

CREATE TABLE `PermissionRoles` (
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `RoleId` int(11) NOT NULL,
  `PermissionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `PermissionRoles`
--

INSERT INTO `PermissionRoles` (`createdAt`, `updatedAt`, `RoleId`, `PermissionId`) VALUES
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 1, 1),
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 1, 2),
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 1, 3),
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 1, 5),
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 1, 6),
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 1, 7),
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 1, 8),
('2022-05-28 06:36:29', '2022-05-28 06:36:29', 1, 10),
('2022-05-28 07:48:42', '2022-05-28 07:48:42', 1, 11),
('2022-05-28 13:11:01', '2022-05-28 13:11:01', 1, 12),
('2022-05-28 07:32:01', '2022-05-28 07:32:01', 1, 13),
('2022-05-28 11:58:21', '2022-05-28 11:58:21', 1, 14),
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 2, 3),
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 2, 7),
('2022-05-28 07:30:59', '2022-05-28 07:30:59', 2, 12),
('2022-05-28 07:30:59', '2022-05-28 07:30:59', 2, 13),
('2022-05-28 11:48:04', '2022-05-28 11:48:04', 2, 14),
('2022-05-27 18:02:44', '2022-05-27 18:02:44', 3, 2),
('2022-06-14 19:35:42', '2022-06-14 19:35:42', 3, 3),
('2022-05-28 06:36:29', '2022-05-28 06:36:29', 3, 9),
('2022-05-28 07:32:01', '2022-05-28 07:32:01', 3, 13),
('2022-05-28 12:13:24', '2022-05-28 12:13:24', 3, 14);

-- --------------------------------------------------------

--
-- Structure de la table `Permissions`
--

CREATE TABLE `Permissions` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Permissions`
--

INSERT INTO `Permissions` (`id`, `nom`, `createdAt`, `updatedAt`) VALUES
(1, 'crud_enseignant', '2022-05-27 17:59:08', '2022-05-27 17:59:08'),
(2, 'crud_livre', '2022-05-27 17:59:08', '2022-05-27 17:59:08'),
(3, 'crud_document', '2022-05-27 17:59:38', '2022-05-27 17:59:38'),
(5, 'bloquer_livre', '2022-05-27 18:00:06', '2022-05-27 18:00:06'),
(6, 'bloquer_document', '2022-05-27 18:00:06', '2022-05-27 18:00:06'),
(7, 'add_domaine', '2022-05-27 18:02:03', '2022-05-27 18:02:03'),
(8, 'delete_domaine', '2022-05-27 18:02:03', '2022-05-27 18:02:03'),
(9, 'crud_etudiant', '2022-05-28 06:35:26', '2022-05-28 06:35:26'),
(10, 'bloquer_user', '2022-05-28 06:35:26', '2022-05-28 06:35:26'),
(11, 'touteRecherche', '2022-05-28 07:20:48', '2022-05-28 07:20:48'),
(12, 'rechercher_etudiant', '2022-05-28 07:20:48', '2022-05-28 07:20:48'),
(13, 'rechercher_livre_document', '2022-05-28 07:30:09', '2022-05-28 07:30:09'),
(14, 'rechercher_enseignant', '2022-05-28 11:47:36', '2022-05-28 11:47:36');

-- --------------------------------------------------------

--
-- Structure de la table `Raisons`
--

CREATE TABLE `Raisons` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Raisons`
--

INSERT INTO `Raisons` (`id`, `description`, `createdAt`, `updatedAt`) VALUES
(1, 'Nudite', '2022-06-20 00:36:35', '2022-06-20 00:36:35'),
(2, 'Contenu Indesirable', '2022-06-20 00:36:35', '2022-06-20 00:36:35'),
(3, 'Violence', '2022-06-20 00:36:35', '2022-06-20 00:36:35'),
(4, 'Harcelement', '2022-06-20 00:36:35', '2022-06-20 00:36:35'),
(5, 'Suicide', '2022-06-20 00:36:35', '2022-06-20 00:36:35'),
(6, 'Fausses Informations', '2022-06-20 00:36:35', '2022-06-20 00:36:35'),
(7, 'Discours Haineux', '2022-06-20 00:36:35', '2022-06-20 00:36:35'),
(8, 'Terrorisme', '2022-06-20 00:36:35', '2022-06-20 00:36:35');

-- --------------------------------------------------------

--
-- Structure de la table `Requetes`
--

CREATE TABLE `Requetes` (
  `id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `UserSenderId` int(11) DEFAULT NULL,
  `DocumentId` int(11) DEFAULT NULL,
  `UserReceiverId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Requetes`
--

INSERT INTO `Requetes` (`id`, `createdAt`, `updatedAt`, `UserSenderId`, `DocumentId`, `UserReceiverId`) VALUES
(1, '2022-06-21 04:32:30', '2022-06-21 04:32:30', 69, 18, 26),
(2, '2022-06-21 09:24:55', '2022-06-21 09:24:55', 69, 18, 10),
(3, '2022-06-21 18:39:29', '2022-06-21 18:39:29', 10, 30, 69),
(5, '2022-06-22 09:54:16', '2022-06-22 09:54:16', 10, 30, 26),
(6, '2022-06-22 10:35:23', '2022-06-22 10:35:23', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Roles`
--

CREATE TABLE `Roles` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Roles`
--

INSERT INTO `Roles` (`id`, `nom`, `createdAt`, `updatedAt`) VALUES
(1, 'admin', '2022-05-27 17:58:13', '2022-05-27 17:58:13'),
(2, 'enseignant', '2022-05-27 17:58:13', '2022-05-27 17:58:13'),
(3, 'etudiant', '2022-05-27 17:58:40', '2022-05-27 17:58:40');

-- --------------------------------------------------------

--
-- Structure de la table `Signalements`
--

CREATE TABLE `Signalements` (
  `id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `DocumentId` int(11) DEFAULT NULL,
  `RaisonId` int(11) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Signalements`
--

INSERT INTO `Signalements` (`id`, `description`, `createdAt`, `updatedAt`, `DocumentId`, `RaisonId`, `UserId`) VALUES
(1, NULL, '2022-06-22 15:24:30', '2022-06-22 15:24:30', 2, 2, 11),
(2, 'contenu inattendu', '2022-06-22 15:26:39', '2022-06-22 15:26:39', 6, NULL, 9),
(3, NULL, '2022-06-22 15:32:01', '2022-06-22 15:32:01', 7, 5, 69),
(4, 'je n\'aime pas tout simplement', '2022-06-22 15:32:37', '2022-06-22 15:32:37', 13, 5, 69);

-- --------------------------------------------------------

--
-- Structure de la table `Specialites`
--

CREATE TABLE `Specialites` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `FiliereId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Specialites`
--

INSERT INTO `Specialites` (`id`, `nom`, `createdAt`, `updatedAt`, `FiliereId`) VALUES
(1, 'genie logiciel', '2022-05-28 16:21:52', '2022-05-28 16:21:52', 1),
(2, 'data science', '2022-05-28 16:21:52', '2022-05-28 16:21:52', 1),
(3, 'sécurité', '2022-05-28 16:34:10', '2022-05-28 16:34:10', 2),
(4, 'Systèmes et réseaux', '2022-05-28 16:34:10', '2022-05-28 16:34:10', 2),
(5, 'BOA', '2022-05-28 16:34:50', '2022-05-28 16:34:50', 3),
(6, 'BOV', '2022-05-28 16:34:50', '2022-05-28 16:34:50', 3);

-- --------------------------------------------------------

--
-- Structure de la table `Types`
--

CREATE TABLE `Types` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Types`
--

INSERT INTO `Types` (`id`, `nom`, `createdAt`, `updatedAt`) VALUES
(1, 'livre', '2022-05-28 15:12:09', '2022-05-28 15:12:09'),
(2, 'cours', '2022-05-28 15:12:09', '2022-05-28 15:12:09'),
(3, 'PV', '2022-05-28 15:14:37', '2022-05-28 15:14:37'),
(4, 'TD', '2022-05-28 15:14:37', '2022-05-28 15:14:37'),
(5, 'TP', '2022-05-28 15:15:40', '2022-05-28 15:15:40'),
(6, 'SN', '2022-05-28 15:15:40', '2022-05-28 15:15:40'),
(7, 'CC', '2022-05-28 15:16:25', '2022-05-28 15:16:25'),
(8, 'rattrapage', '2022-05-28 15:16:25', '2022-05-28 15:16:25'),
(9, 'correction', '2022-05-28 15:16:44', '2022-05-28 15:16:44');

-- --------------------------------------------------------

--
-- Structure de la table `Ues`
--

CREATE TABLE `Ues` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `intitule` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `FiliereId` int(11) DEFAULT NULL,
  `NiveauId` int(11) DEFAULT NULL,
  `SpecialiteId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Ues`
--

INSERT INTO `Ues` (`id`, `nom`, `intitule`, `createdAt`, `updatedAt`, `FiliereId`, `NiveauId`, `SpecialiteId`) VALUES
(1, 'info3041', 'Introduction à l\'algorithmique', '2022-06-05 22:28:52', '2022-06-05 22:28:52', NULL, NULL, NULL),
(2, 'inf3046', 'SE et Reseau', '2022-06-05 22:28:52', '2022-06-05 22:28:52', NULL, NULL, NULL),
(3, 'inf3075', 'software testing', '2022-06-05 23:33:34', '2022-06-05 23:33:34', 1, 3, 1),
(4, 'inf3025 ', 'Calcul Scientifique', '2022-06-13 15:51:35', '2022-06-13 15:51:35', 1, 3, NULL),
(5, 'eng3035\r\n', 'English for Mathematics and Computer Science II', '2022-06-13 15:52:54', '2022-06-13 15:52:54', 1, 3, NULL),
(6, 'fra3045 \r\n', 'Français pour Sciences Mathématiques et\r\nInformatiques II', '2022-06-13 15:54:07', '2022-06-13 15:54:07', 1, 3, NULL),
(7, 'inf3055 ', 'Conception Orientée Objet', '2022-06-13 15:54:50', '2022-06-13 15:54:50', 1, 3, 1),
(8, 'inf3065 ', 'Systèmes d’information', '2022-06-13 15:55:23', '2022-06-13 15:55:23', 1, 3, 1),
(9, 'inf3085 \r\n', 'Réseaux I', '2022-06-13 15:55:56', '2022-06-13 15:55:56', 1, 3, 4),
(10, 'inf3095 \r\n', 'Administration système', '2022-06-13 15:56:26', '2022-06-13 15:56:26', 1, 3, 4),
(11, 'inf3105 ', 'Services réseaux', '2022-06-13 15:57:33', '2022-06-13 15:57:33', 1, 3, 4),
(12, 'inf3115 ', 'Fouille de Données I', '2022-06-13 15:58:06', '2022-06-13 15:58:06', 1, 3, 2),
(13, 'inf3125 ', 'Analyses Statistiques', '2022-06-13 15:58:47', '2022-06-13 15:58:47', 1, 3, 2),
(14, 'inf3135 \r\n', 'Techniques d\'Optimisation I', '2022-06-13 15:59:51', '2022-06-13 15:59:51', 1, 3, 2),
(15, 'inf3145 \r\n', 'Cryptographie Classique', '2022-06-13 16:01:42', '2022-06-13 16:01:42', 1, 3, 3),
(16, 'inf3155 ', 'Théorie de Shannon', '2022-06-13 16:02:14', '2022-06-13 16:02:14', 1, 3, 3),
(17, 'inf3165 \r\n', 'Théorie des Codes 1', '2022-06-13 16:02:56', '2022-06-13 16:02:56', 1, 3, 3),
(18, 'inf1011', 'Introduction à l’algorithmique', '2022-06-13 17:38:47', '2022-06-13 17:38:47', 1, 1, NULL),
(19, 'inf1021', 'Introduction à l’Architecture des\r\nordinateurs', '2022-06-13 17:39:28', '2022-06-13 17:39:28', 1, 1, NULL),
(20, 'inf1031', 'Programmation structurée en C', '2022-06-13 17:40:18', '2022-06-13 17:40:18', 1, 1, NULL),
(21, 'mat1041\r\n', 'Algèbre 1', '2022-06-13 17:40:46', '2022-06-13 17:40:46', 1, 1, NULL),
(22, 'inf1051 ', 'Fondements Mathématiques de \r\nl’Informatique', '2022-06-13 17:41:38', '2022-06-13 17:41:38', 1, 1, NULL),
(23, 'phy151 ', 'Électronique numérique', '2022-06-13 17:43:28', '2022-06-13 17:43:28', 1, 1, NULL),
(24, 'inf1042 \r\n', 'Introduction aux Structures de données', '2022-06-13 17:44:04', '2022-06-13 17:44:04', 1, 1, NULL),
(25, 'inf1052', 'Introduction aux réseaux', '2022-06-13 17:45:18', '2022-06-13 17:45:18', 1, 1, NULL),
(26, 'inf1062 \r\n', 'Introduction aux systèmes d’exploitation', '2022-06-13 17:45:45', '2022-06-13 17:45:45', 1, 1, NULL),
(27, 'mat1052 ', 'Analyse 1', '2022-06-13 17:46:19', '2022-06-13 17:46:19', 1, 1, NULL),
(28, 'eng152 ', 'English for Mathematics and Computer Sciences I', '2022-06-13 17:46:49', '2022-06-13 17:46:49', 1, 1, NULL),
(29, 'fra152 \r\n', 'Français pour Sciences Mathématiques et\r\nInformatiques I', '2022-06-13 17:47:24', '2022-06-13 17:47:24', 1, 1, NULL),
(30, 'ppe1011 ', 'Projet Personnel Étudiant', '2022-06-13 17:49:10', '2022-06-13 17:49:10', 1, 1, NULL),
(31, 'inf2013 ', 'Introduction au Génie Logiciel et\r\nSystème d’Information', '2022-06-13 17:49:53', '2022-06-13 17:49:53', 1, 2, NULL),
(32, 'inf2023 \r\n', 'Introduction aux Bases de Données', '2022-06-13 17:50:38', '2022-06-13 17:50:38', 1, 2, NULL),
(33, 'inf2033 ', 'Méthodes Algorithmiques et Structures\r\nde Données', '2022-06-13 17:51:08', '2022-06-13 17:51:08', 1, 2, NULL),
(34, 'mat2053 \r\n', 'Algèbre 2', '2022-06-13 17:51:39', '2022-06-13 17:51:39', 1, 2, NULL),
(35, 'inf2063 \r\n', 'Architecture des Ordinateurs', '2022-06-13 17:52:04', '2022-06-13 17:52:04', 1, 2, NULL),
(36, 'inf2073 \r\n', 'Systèmes d’exploitation et réseaux I', '2022-06-13 17:52:49', '2022-06-13 17:52:49', 1, 2, NULL),
(37, 'inf2044 ', 'Statistiques et Analyse de Données', '2022-06-13 17:53:19', '2022-06-13 17:53:19', 1, 2, NULL),
(38, 'inf2054 ', 'Modélisation et Programmation Orientée\r\nObjet ', '2022-06-13 17:54:15', '2022-06-13 17:54:15', 1, 2, NULL),
(39, 'inf2064 ', 'Programmation Web', '2022-06-13 17:57:59', '2022-06-13 17:57:59', 1, 2, NULL),
(40, 'mat2124 \r\n', 'Analyse 2', '2022-06-13 17:59:57', '2022-06-13 17:59:57', 1, 2, NULL),
(41, 'eng2144 ', 'English for Mathematics and Computer\r\nScience II', '2022-06-13 18:00:19', '2022-06-13 18:00:19', 1, 2, NULL),
(42, 'fra2134 ', 'Français pour Sciences Mathématiques et\r\nInformatiques II', '2022-06-13 18:00:58', '2022-06-13 18:00:58', 1, 2, NULL),
(43, 'PPE2xx4 ', 'PPE ', '2022-06-13 18:01:31', '2022-06-13 18:01:31', 1, 2, NULL),
(44, 'inf3036 ', 'Bases de Données', '2022-06-13 18:02:50', '2022-06-13 18:02:50', 1, 3, NULL),
(45, 'inf3046 \r\n', 'Systèmes d’exploitation et Réseaux II', '2022-06-13 18:03:15', '2022-06-13 18:03:15', 1, 3, NULL),
(46, 'PPE3xx6 ', 'PPE', '2022-06-13 18:04:07', '2022-06-13 18:04:07', 1, 3, NULL),
(47, 'inf3176 ', 'Techniques de Programmation Avancée', '2022-06-13 18:04:31', '2022-06-13 18:04:31', 1, 3, 1),
(48, 'inf3186 \r\n', 'Systèmes d\'information décisionnels', '2022-06-13 18:05:06', '2022-06-13 18:05:06', 1, 3, 1),
(49, 'inf3196 \r\n', 'Projet I', '2022-06-13 18:06:04', '2022-06-13 18:06:04', 1, 3, 1),
(50, 'inf3206 \r\n', 'Programmation Orientée Système', '2022-06-13 18:06:28', '2022-06-13 18:06:28', 1, 3, 4),
(51, 'inf3216 ', 'Administration et supervision Réseau\r\n', '2022-06-13 18:07:46', '2022-06-13 18:07:46', 1, 3, 4),
(52, 'inf3226 \r\n', 'Projet I', '2022-06-13 18:08:20', '2022-06-13 18:08:20', 1, 3, 4),
(53, 'inf3236 ', 'Apprentissage artificiel I', '2022-06-13 18:09:00', '2022-06-13 18:09:00', 1, 3, 2),
(54, 'inf3246', 'Programmation Logique', '2022-06-13 18:09:44', '2022-06-13 18:09:44', 1, 3, 2),
(55, 'inf3256 ', 'Projet I', '2022-06-13 18:10:25', '2022-06-13 18:10:25', 1, 3, 2),
(56, 'inf3266 ', 'Théorie des codes 2', '2022-06-13 18:10:54', '2022-06-13 18:10:54', 1, 3, 3),
(57, 'inf3276 \r\n', 'Théorie des Nombres 1', '2022-06-13 18:13:13', '2022-06-13 18:13:13', 1, 3, 3),
(58, 'inf3286 \r\n', 'Projet I', '2022-06-13 18:13:51', '2022-06-13 18:13:51', 1, 3, 3),
(59, 'inf4017 ', 'Complexité et algorithmique avancée', '2022-06-13 18:16:06', '2022-06-13 18:16:06', 1, 4, NULL),
(60, 'inf4027 \r\n', 'Génie logiciel', '2022-06-13 18:16:40', '2022-06-13 18:16:40', 1, 4, NULL),
(61, 'inf4057 ', 'Architectures Logicielles', '2022-06-13 18:17:09', '2022-06-13 18:17:09', 1, 4, 1),
(62, 'inf4067 ', 'UML et Design Patterns', '2022-06-13 18:17:42', '2022-06-13 18:17:42', 1, 4, 1),
(63, 'inf4077 ', 'Programmation des terminaux mobiles', '2022-06-13 18:18:24', '2022-06-13 18:18:24', 1, 4, 1),
(64, 'inf4087 ', 'Réseaux II', '2022-06-13 18:18:58', '2022-06-13 18:18:58', 1, 4, 4),
(65, 'inf4097 ', 'Principes de conception des systèmes\r\nd’exploitation', '2022-06-13 18:19:40', '2022-06-13 18:19:40', 1, 4, 4),
(66, 'inf4107 \r\n', 'Cloud computing', '2022-06-13 18:20:11', '2022-06-13 18:20:11', 1, 4, 4),
(67, 'inf4117 ', 'Fouille de données II', '2022-06-13 18:20:42', '2022-06-13 18:20:42', 1, 4, 2),
(68, 'inf4127 \r\n', 'Techniques d’optimisation II', '2022-06-13 18:21:45', '2022-06-13 18:21:45', 1, 4, 2),
(69, 'inf4137 ', 'Analyse des données', '2022-06-13 18:22:25', '2022-06-13 18:22:25', 1, 4, 2),
(70, 'inf4147 ', 'Sécurité Informatique', '2022-06-13 18:23:10', '2022-06-13 18:23:10', 1, 4, 3),
(71, 'inf4157 ', 'Sécurité Logicielle', '2022-06-13 18:23:34', '2022-06-13 18:23:34', 1, 4, 3),
(72, 'inf4167 ', 'Cryptographie symétrique', '2022-06-13 18:24:16', '2022-06-13 18:24:16', 1, 4, 3),
(73, 'inf4038 ', 'Base de données distribuées', '2022-06-13 18:24:43', '2022-06-13 18:24:43', 1, 4, NULL),
(74, 'inf4048 ', 'Compilation', '2022-06-13 18:25:31', '2022-06-13 18:25:31', 1, 4, NULL),
(75, 'inf4178 ', 'Génie Logiciel I', '2022-06-13 18:26:10', '2022-06-13 18:26:10', 1, 4, 1),
(76, 'inf4188 ', 'Web sémantique et applications', '2022-06-13 18:26:40', '2022-06-13 18:26:40', 1, 4, 1),
(77, 'inf4198 ', 'Projet II', '2022-06-13 18:27:12', '2022-06-13 18:27:12', 1, 4, 1),
(78, 'inf4208 ', 'Réseaux mobiles et sans fils', '2022-06-13 18:27:43', '2022-06-13 18:27:43', 1, 4, 4),
(79, 'inf4218 ', 'Programmation distribuée', '2022-06-13 18:28:14', '2022-06-13 18:28:14', 1, 4, 4),
(80, 'inf4228 ', 'Projet II', '2022-06-13 18:28:56', '2022-06-13 18:28:56', 1, 4, 4),
(81, 'inf4238 ', 'Systèmes experts', '2022-06-13 18:29:45', '2022-06-13 18:29:45', 1, 4, 2),
(82, 'inf4248 ', 'Apprentissage artificiel II', '2022-06-13 18:30:10', '2022-06-13 18:30:10', 1, 4, 2),
(83, 'inf4258 ', 'Projet II', '2022-06-13 18:30:39', '2022-06-13 18:30:39', 1, 4, 2),
(84, 'inf4268 ', 'Cryptographie Asymétrique ', '2022-06-13 18:31:22', '2022-06-13 18:31:22', 1, 4, 3),
(85, 'inf4278 ', 'Courbes Elliptiques 1', '2022-06-13 18:31:48', '2022-06-13 18:31:48', 1, 4, 3),
(86, 'inf4288 ', 'Projet II', '2022-06-13 18:32:22', '2022-06-13 18:32:22', 1, 4, 3),
(87, 'inf5019 \r\n', 'Veille scientifique et technologique', '2022-06-13 18:32:56', '2022-06-13 18:32:56', 1, 5, NULL),
(88, 'inf5029 ', 'Science de données', '2022-06-13 18:34:22', '2022-06-13 18:34:22', 1, 5, NULL),
(89, 'inf5039 ', 'Ingénierie Dirigée par les Modèles\r\n', '2022-06-13 18:35:58', '2022-06-13 18:35:58', 1, 5, 1),
(90, 'inf5049 \r\n', 'Big Data', '2022-06-13 18:36:31', '2022-06-13 18:36:31', 1, 5, 1),
(91, 'inf5059 \r\n', 'Génie logiciel II', '2022-06-13 18:37:13', '2022-06-13 18:37:13', 1, 5, 1),
(92, 'inf5069 \r\n', 'Internet des Objets', '2022-06-13 18:38:03', '2022-06-13 18:38:03', 1, 5, 4),
(93, 'inf5079 \r\n', 'Programmation multi-cœurs', '2022-06-13 18:38:45', '2022-06-13 18:38:45', 1, 5, 4),
(94, 'inf5089 ', 'Sécurité et audit des réseaux', '2022-06-13 18:39:14', '2022-06-13 18:39:14', 1, 5, 4),
(95, 'inf5099 ', 'Apprentissage distribué', '2022-06-13 18:39:50', '2022-06-13 18:39:50', 1, 5, 2),
(96, 'inf5109 ', 'Systèmes multi-agents', '2022-06-13 18:40:31', '2022-06-13 18:40:31', 1, 5, 2),
(97, 'inf5119 \r\n', 'Techniques avancées de fouille de\r\ndonnées et applications', '2022-06-13 18:41:13', '2022-06-13 18:41:13', 1, 5, 2),
(98, 'inf5129 \r\n', 'Cyberattaque & Cyberdéfense', '2022-06-13 18:41:48', '2022-06-13 18:41:48', 1, 5, 3),
(99, 'inf5139 \r\n', 'Courbes Elliptiques 2', '2022-06-13 18:42:21', '2022-06-13 18:42:21', 1, 5, 3),
(100, 'inf5149 \r\n', 'Cryptographie basée sur les codes', '2022-06-13 18:43:06', '2022-06-13 18:43:06', 1, 5, 3),
(101, 'inf5009 ', 'Mémoire de Recherche', '2022-06-13 18:45:04', '2022-06-13 18:45:04', 1, 5, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `RoleId` int(11) DEFAULT NULL,
  `etat` varchar(255) NOT NULL DEFAULT 'actif',
  `FaculteId` int(11) DEFAULT NULL,
  `FiliereId` int(11) DEFAULT NULL,
  `NiveauId` int(11) DEFAULT NULL,
  `SpecialiteId` int(11) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  `username` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Users`
--

INSERT INTO `Users` (`id`, `email`, `password`, `createdAt`, `updatedAt`, `RoleId`, `etat`, `FaculteId`, `FiliereId`, `NiveauId`, `SpecialiteId`, `UserId`, `username`) VALUES
(2, 'enseigant11@yahoo.com', '$2a$10$IBX/8Dwe67kpcH1K/1kxbuXj0A20.9qS7EhUoKTA6EmrE8lWS/wNq', '2022-05-27 17:31:06', '2022-06-07 11:00:16', 2, 'bloqué', 1, 3, 3, 5, NULL, 'enseignant'),
(3, 'enseigant12@yahoo.com', '$2a$10$DUrHNsb1F5ykdKEmwpml6.DsNqmTAImDSFSzvzjOiUGJIWebge066', '2022-05-27 17:33:26', '2022-06-06 15:47:02', 3, 'bloqué', NULL, NULL, NULL, NULL, NULL, ''),
(4, 'ensens@yahoo.com', '$2a$10$teZlA8MujnpbdbBf97XSoeqfi9.PnaTVHyuiVecBFwuX6z8dbKMo.', '2022-05-27 17:41:27', '2022-05-27 17:41:27', 3, 'bloqué', NULL, NULL, NULL, NULL, NULL, ''),
(5, 'ensens1@yahoo.com', '$2a$10$Btoveeivgn8XHQPO0hYMjeMt0iDtYl1BoY5B0ZUH/UwQjCaQPdiA2', '2022-05-27 17:47:00', '2022-05-27 17:47:00', 3, 'bloqué', NULL, NULL, NULL, NULL, NULL, ''),
(6, 'ensens11@yahoo.com', '$2a$10$lZl4giuziXYZzSLQHoOXnO7UNWLlzXo0iKDgwFcWrOc.joQ0B2Qk2', '2022-05-27 17:48:08', '2022-05-27 17:48:08', 3, 'bloqué', NULL, NULL, NULL, NULL, NULL, ''),
(7, 'enseigant123@yahoo.com', '$2a$10$MCvpHvpqpC5Zz6ExDyY9belvawPwOSgRJRCUXUEzw29eHWJyjW4Pq', '2022-05-27 17:48:59', '2022-06-06 16:01:34', 3, 'bloqué', NULL, NULL, NULL, NULL, NULL, ''),
(8, 'admin2@yahoo.com', '$2a$10$cLo4.4Y71hkODP09/WQEje6hMorle4dtFPWaeuTXbPQKFy9jHTBk2', '2022-05-28 03:04:55', '2022-05-28 03:04:55', 1, 'actif', NULL, NULL, NULL, NULL, NULL, ''),
(9, 'ens2@yahoo.com', '$2a$10$ICbKA2/0UjWW6YmnFO0npuB7q8fj4KSOwdD5WYQ/Z0h/ibCrAw6EC', '2022-05-28 03:06:06', '2022-05-28 03:06:06', 2, 'actif', NULL, NULL, NULL, NULL, NULL, 'ens2'),
(10, 'enss2@yahoo.com', '$2a$10$oqty0noFQs4oztUBZNxIgODdJBGzILcoB9G1esxsF8FM/1NtnS/aK', '2022-05-28 03:18:35', '2022-05-28 03:18:35', 2, 'actif', NULL, NULL, NULL, NULL, NULL, 'enseignant'),
(11, 'enseignant22@yahoo.com', '$2a$10$RPJBk4qu9z15ZI8774mM2uxN/Ai1JilyBpjYPxH5ye1VQR0VHsxEy', '2022-05-28 03:37:02', '2022-05-28 03:37:02', 2, 'actif', NULL, NULL, NULL, NULL, NULL, 'enseignant22'),
(12, 'registerenseignant@yahoo.com', '$2a$10$4uJBy8ecXbMY1ZL4f2CJb.Aqr7ph/Pi2Yf/P3zmfb7yg86LxU5oM.', '2022-05-28 04:13:37', '2022-05-28 04:13:37', 2, 'actif', NULL, NULL, NULL, NULL, NULL, ''),
(18, 'registerEtudiant4@yahoo.com', '$2a$10$Wq0H/iXycJ/upB4xvUeeCOvsKdau4rgwpvgXbiRztMlH4vE8Enoaq', '2022-05-28 04:41:37', '2022-06-06 15:20:49', 3, 'actif', NULL, NULL, NULL, NULL, NULL, 'userss'),
(26, 'paulette@yahoo.com', '$2a$10$JH3WCL.OD.GO.ysccB8hQeA8Qquy4d8SCfB.nitnxtQwLeFWK/qbG', '2022-05-28 18:22:55', '2022-05-28 18:22:55', 2, 'actif', 1, 1, 3, 1, NULL, 'paulette'),
(57, 'yeki@yahoo.com', '123456789', '2022-06-04 09:38:40', '2022-06-07 18:00:53', 1, 'actif', 1, 1, 3, NULL, NULL, 'yeki'),
(60, 'paupina@yahoo.com', '$2a$10$X2gZRHOu/VEGtqlN72bdLuP4AFxEJFa.ZCXcPHS9HnmCzUdscLopK', '2022-06-07 15:33:39', '2022-06-07 15:33:39', 3, 'actif', 1, 2, 2, 1, NULL, 'paupina'),
(61, 'paupau@yahoo.com', '$2a$10$8z2M/YChbXhJEBIcQ1baDuyjaeH/sSdJZv5YdrWUU9GHCRu8qgjti', '2022-06-07 15:37:55', '2022-06-07 15:37:55', 3, 'actif', 1, 2, 5, 3, NULL, 'paupau'),
(62, 'valeryMonthe@fasciences-uy1.cm', '$2a$10$1SypBLdMEWwZ.14xMd8zu.a/aw0SZmrlS76TeubeCXoju9Lu3zjyG', '2022-06-11 16:17:35', '2022-06-11 16:17:36', 2, 'actif', 1, 1, 3, NULL, NULL, 'Monthe Valery'),
(63, 'jiomekong@fasciences-uy1.cm', '$2a$10$YRL64lBaofF30ERfHk0oGO2VvCJ9brb.al6AEy3Om3HqaQ.uTvsXG', '2022-06-11 16:24:56', '2022-06-11 16:24:56', 2, 'actif', 1, 1, 4, NULL, NULL, 'Jiomekong fidel'),
(64, 'melatagia@fasciences-uy1.cm', '$2a$10$CZaf/KOkAtURS21g7jAIE.aAKXuu9z0Ktln/gmeE9Gp223L6N4UzS', '2022-06-11 16:30:08', '2022-06-11 16:30:08', 2, 'actif', 1, 1, 6, NULL, NULL, 'Melatagia Paulin'),
(65, 'tsopze@fasciences-uy1.cm', '$2a$10$XncuKfDG2xV9mwJosvVAZ.lABoBqIqDQ3SniobDO9EinzeAjSfBmy', '2022-06-11 16:35:14', '2022-06-11 16:35:14', 2, 'actif', 1, 1, 5, NULL, NULL, 'Tsopze Nobert'),
(67, 'xaveria.kimbi@facsciences-uy1.cm', '$2a$10$/wQ9fd13cxYsoLEmHC/VtOxJucTNEif95ZE4LhSfVPOj1TaXaPFdO', '2022-06-11 16:56:35', '2022-06-11 16:56:35', 3, 'actif', 1, 1, 3, 1, NULL, 'Kimbi Xaviera'),
(68, 'user1@yahoo.com', '$2a$10$Y8.KSGllESDPy/jI1eOfy.v9QbJvSuCeihgEyzEVnpGrv6oNG7qgW', '2022-06-14 05:55:36', '2022-06-14 05:55:36', 3, 'actif', 1, 1, 4, 1, NULL, 'user1'),
(69, 'nsangou.adamou.20249@facsciences-uy1.cm', '$2a$10$JH3WCL.OD.GO.ysccB8hQeA8Qquy4d8SCfB.nitnxtQwLeFWK/qbG', '2022-06-14 17:22:21', '2022-06-14 17:22:22', 1, 'actif', 1, 1, 3, 1, NULL, 'Nsangou Adamou'),
(70, 'adamoun.505@gmail.com', '$2a$10$fdoQuOgAE3N1PmkPWSjgCeMlppNadQ5JJcqvtk.9JhjBzX36nNZem', '2022-06-15 10:58:50', '2022-06-15 10:58:50', 3, 'actif', 1, 1, 3, 2, NULL, 'Adams');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Corrections`
--
ALTER TABLE `Corrections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idEnseignantPublieur` (`idEnseignantPublieur`),
  ADD KEY `EnseignantId` (`EnseignantId`),
  ADD KEY `idEnseignantSupprimeur` (`idEnseignantSupprimeur`),
  ADD KEY `TdId` (`TdId`),
  ADD KEY `EpreuveId` (`EpreuveId`);

--
-- Index pour la table `DocumentDomaine`
--
ALTER TABLE `DocumentDomaine`
  ADD PRIMARY KEY (`DomaineId`,`DocumentId`),
  ADD KEY `DocumentId` (`DocumentId`);

--
-- Index pour la table `Documents`
--
ALTER TABLE `Documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `TypeId` (`TypeId`),
  ADD KEY `FaculteId` (`FaculteId`),
  ADD KEY `FiliereId` (`FiliereId`),
  ADD KEY `NiveauId` (`NiveauId`),
  ADD KEY `SpecialiteId` (`SpecialiteId`),
  ADD KEY `UserId` (`UserId`),
  ADD KEY `UeId` (`UeId`);

--
-- Index pour la table `Domaines`
--
ALTER TABLE `Domaines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `UserId` (`UserId`);

--
-- Index pour la table `Epreuves`
--
ALTER TABLE `Epreuves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idEnseignantPublieur` (`idEnseignantPublieur`),
  ADD KEY `EnseignantId` (`EnseignantId`),
  ADD KEY `idEnseignantSupprimeur` (`idEnseignantSupprimeur`);

--
-- Index pour la table `Facultes`
--
ALTER TABLE `Facultes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `filiereNiveau`
--
ALTER TABLE `filiereNiveau`
  ADD PRIMARY KEY (`NiveauId`,`SpecialiteId`),
  ADD KEY `SpecialiteId` (`SpecialiteId`);

--
-- Index pour la table `Filieres`
--
ALTER TABLE `Filieres`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FaculteId` (`FaculteId`);

--
-- Index pour la table `Messages`
--
ALTER TABLE `Messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `UserId` (`UserId`),
  ADD KEY `RequeteId` (`RequeteId`);

--
-- Index pour la table `Niveaus`
--
ALTER TABLE `Niveaus`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Notifications`
--
ALTER TABLE `Notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Notifications_UserId_foreign_idx` (`UserId`),
  ADD KEY `Notifications_FiliereId_foreign_idx` (`FiliereId`);

--
-- Index pour la table `PermissionRoles`
--
ALTER TABLE `PermissionRoles`
  ADD PRIMARY KEY (`RoleId`,`PermissionId`),
  ADD KEY `PermissionId` (`PermissionId`);

--
-- Index pour la table `Permissions`
--
ALTER TABLE `Permissions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Raisons`
--
ALTER TABLE `Raisons`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Requetes`
--
ALTER TABLE `Requetes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `UserSenderId` (`UserSenderId`),
  ADD KEY `DocumentId` (`DocumentId`),
  ADD KEY `UserReceiverId` (`UserReceiverId`);

--
-- Index pour la table `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Signalements`
--
ALTER TABLE `Signalements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `DocumentId` (`DocumentId`),
  ADD KEY `RaisonId` (`RaisonId`),
  ADD KEY `UserId` (`UserId`);

--
-- Index pour la table `Specialites`
--
ALTER TABLE `Specialites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FiliereId` (`FiliereId`);

--
-- Index pour la table `Types`
--
ALTER TABLE `Types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Ues`
--
ALTER TABLE `Ues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FiliereId` (`FiliereId`),
  ADD KEY `NiveauId` (`NiveauId`),
  ADD KEY `SpecialiteId` (`SpecialiteId`);

--
-- Index pour la table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `email_2` (`email`),
  ADD UNIQUE KEY `email_3` (`email`),
  ADD UNIQUE KEY `email_4` (`email`),
  ADD UNIQUE KEY `email_5` (`email`),
  ADD UNIQUE KEY `email_6` (`email`),
  ADD UNIQUE KEY `email_7` (`email`),
  ADD UNIQUE KEY `email_8` (`email`),
  ADD UNIQUE KEY `email_9` (`email`),
  ADD UNIQUE KEY `email_10` (`email`),
  ADD UNIQUE KEY `email_11` (`email`),
  ADD UNIQUE KEY `email_12` (`email`),
  ADD UNIQUE KEY `email_13` (`email`),
  ADD UNIQUE KEY `email_14` (`email`),
  ADD UNIQUE KEY `email_15` (`email`),
  ADD UNIQUE KEY `email_16` (`email`),
  ADD UNIQUE KEY `email_17` (`email`),
  ADD UNIQUE KEY `email_18` (`email`),
  ADD UNIQUE KEY `email_19` (`email`),
  ADD UNIQUE KEY `email_20` (`email`),
  ADD UNIQUE KEY `email_21` (`email`),
  ADD UNIQUE KEY `email_22` (`email`),
  ADD UNIQUE KEY `email_23` (`email`),
  ADD UNIQUE KEY `email_24` (`email`),
  ADD UNIQUE KEY `email_25` (`email`),
  ADD UNIQUE KEY `email_26` (`email`),
  ADD UNIQUE KEY `email_27` (`email`),
  ADD UNIQUE KEY `email_28` (`email`),
  ADD UNIQUE KEY `email_29` (`email`),
  ADD UNIQUE KEY `email_30` (`email`),
  ADD UNIQUE KEY `email_31` (`email`),
  ADD UNIQUE KEY `email_32` (`email`),
  ADD UNIQUE KEY `email_33` (`email`),
  ADD UNIQUE KEY `email_34` (`email`),
  ADD UNIQUE KEY `email_35` (`email`),
  ADD KEY `RoleId` (`RoleId`),
  ADD KEY `FaculteId` (`FaculteId`),
  ADD KEY `FiliereId` (`FiliereId`),
  ADD KEY `NiveauId` (`NiveauId`),
  ADD KEY `SpecialiteId` (`SpecialiteId`),
  ADD KEY `UserId` (`UserId`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Corrections`
--
ALTER TABLE `Corrections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Documents`
--
ALTER TABLE `Documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pour la table `Domaines`
--
ALTER TABLE `Domaines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `Epreuves`
--
ALTER TABLE `Epreuves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Facultes`
--
ALTER TABLE `Facultes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Filieres`
--
ALTER TABLE `Filieres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `Messages`
--
ALTER TABLE `Messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `Niveaus`
--
ALTER TABLE `Niveaus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `Notifications`
--
ALTER TABLE `Notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `Permissions`
--
ALTER TABLE `Permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `Raisons`
--
ALTER TABLE `Raisons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `Requetes`
--
ALTER TABLE `Requetes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `Roles`
--
ALTER TABLE `Roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `Signalements`
--
ALTER TABLE `Signalements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `Specialites`
--
ALTER TABLE `Specialites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `Types`
--
ALTER TABLE `Types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `Ues`
--
ALTER TABLE `Ues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT pour la table `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Corrections`
--
ALTER TABLE `Corrections`
  ADD CONSTRAINT `Corrections_ibfk_16` FOREIGN KEY (`idEnseignantPublieur`) REFERENCES `Enseignants` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Corrections_ibfk_17` FOREIGN KEY (`EnseignantId`) REFERENCES `Enseignants` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Corrections_ibfk_18` FOREIGN KEY (`idEnseignantSupprimeur`) REFERENCES `Enseignants` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Corrections_ibfk_19` FOREIGN KEY (`TdId`) REFERENCES `Tds` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Corrections_ibfk_20` FOREIGN KEY (`EpreuveId`) REFERENCES `Epreuves` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `DocumentDomaine`
--
ALTER TABLE `DocumentDomaine`
  ADD CONSTRAINT `DocumentDomaine_ibfk_1` FOREIGN KEY (`DomaineId`) REFERENCES `Domaines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `DocumentDomaine_ibfk_2` FOREIGN KEY (`DocumentId`) REFERENCES `Documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Documents`
--
ALTER TABLE `Documents`
  ADD CONSTRAINT `Documents_ibfk_194` FOREIGN KEY (`TypeId`) REFERENCES `Types` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Documents_ibfk_195` FOREIGN KEY (`FaculteId`) REFERENCES `Facultes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Documents_ibfk_196` FOREIGN KEY (`FiliereId`) REFERENCES `Filieres` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Documents_ibfk_197` FOREIGN KEY (`NiveauId`) REFERENCES `Niveaus` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Documents_ibfk_198` FOREIGN KEY (`SpecialiteId`) REFERENCES `Specialites` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Documents_ibfk_199` FOREIGN KEY (`UserId`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Documents_ibfk_200` FOREIGN KEY (`UeId`) REFERENCES `Ues` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Domaines`
--
ALTER TABLE `Domaines`
  ADD CONSTRAINT `Domaines_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Epreuves`
--
ALTER TABLE `Epreuves`
  ADD CONSTRAINT `Epreuves_ibfk_1` FOREIGN KEY (`idEnseignantPublieur`) REFERENCES `Enseignants` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Epreuves_ibfk_2` FOREIGN KEY (`EnseignantId`) REFERENCES `Enseignants` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Epreuves_ibfk_3` FOREIGN KEY (`idEnseignantSupprimeur`) REFERENCES `Enseignants` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `filiereNiveau`
--
ALTER TABLE `filiereNiveau`
  ADD CONSTRAINT `filiereNiveau_ibfk_1` FOREIGN KEY (`NiveauId`) REFERENCES `Niveaus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `filiereNiveau_ibfk_2` FOREIGN KEY (`SpecialiteId`) REFERENCES `Specialites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Filieres`
--
ALTER TABLE `Filieres`
  ADD CONSTRAINT `Filieres_ibfk_1` FOREIGN KEY (`FaculteId`) REFERENCES `Facultes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Messages`
--
ALTER TABLE `Messages`
  ADD CONSTRAINT `Messages_ibfk_19` FOREIGN KEY (`UserId`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Messages_ibfk_20` FOREIGN KEY (`RequeteId`) REFERENCES `Requetes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Notifications`
--
ALTER TABLE `Notifications`
  ADD CONSTRAINT `Notifications_FiliereId_foreign_idx` FOREIGN KEY (`FiliereId`) REFERENCES `Filieres` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Notifications_UserId_foreign_idx` FOREIGN KEY (`UserId`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `PermissionRoles`
--
ALTER TABLE `PermissionRoles`
  ADD CONSTRAINT `PermissionRoles_ibfk_1` FOREIGN KEY (`RoleId`) REFERENCES `Roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `PermissionRoles_ibfk_2` FOREIGN KEY (`PermissionId`) REFERENCES `Permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Requetes`
--
ALTER TABLE `Requetes`
  ADD CONSTRAINT `Requetes_ibfk_7` FOREIGN KEY (`UserSenderId`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Requetes_ibfk_8` FOREIGN KEY (`DocumentId`) REFERENCES `Documents` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Requetes_ibfk_9` FOREIGN KEY (`UserReceiverId`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Signalements`
--
ALTER TABLE `Signalements`
  ADD CONSTRAINT `Signalements_ibfk_26` FOREIGN KEY (`DocumentId`) REFERENCES `Documents` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Signalements_ibfk_27` FOREIGN KEY (`RaisonId`) REFERENCES `Raisons` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Signalements_ibfk_28` FOREIGN KEY (`UserId`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Specialites`
--
ALTER TABLE `Specialites`
  ADD CONSTRAINT `Specialites_ibfk_1` FOREIGN KEY (`FiliereId`) REFERENCES `Filieres` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Ues`
--
ALTER TABLE `Ues`
  ADD CONSTRAINT `Ues_ibfk_43` FOREIGN KEY (`FiliereId`) REFERENCES `Filieres` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Ues_ibfk_44` FOREIGN KEY (`NiveauId`) REFERENCES `Niveaus` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Ues_ibfk_45` FOREIGN KEY (`SpecialiteId`) REFERENCES `Specialites` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Users`
--
ALTER TABLE `Users`
  ADD CONSTRAINT `Users_ibfk_166` FOREIGN KEY (`RoleId`) REFERENCES `Roles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Users_ibfk_167` FOREIGN KEY (`FaculteId`) REFERENCES `Facultes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Users_ibfk_168` FOREIGN KEY (`FiliereId`) REFERENCES `Filieres` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Users_ibfk_169` FOREIGN KEY (`NiveauId`) REFERENCES `Niveaus` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Users_ibfk_170` FOREIGN KEY (`SpecialiteId`) REFERENCES `Specialites` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Users_ibfk_171` FOREIGN KEY (`UserId`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
